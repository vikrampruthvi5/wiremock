import requests
from requests import get, post

if __name__ == "__main__":
    while True:
        print("1: Get users\n2: Post user\n3: Exit application")
        selection = int(input("(1/2/3): "))
        if selection == 1:
            print("Making GET call...\n")
            response = requests.get("http://localhost:8080/users")
            print(f'response: {response}\nstatuscode: {response.status_code}')
            # printing response data
            data = response.json()
            print(f'{data}\n')
        elif selection == 1:
            print("Making GET call")
        elif selection == 3:
            print("Exiting application...Bye!")
            exit(0)
        else:
            print("Wrong option selected... Please re try.")